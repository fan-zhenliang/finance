个人理财系统（springboot+thymeleaf）

一、介绍
    该项目是要时针对个人的理财系统。对于用户主要分为四个模块：登录/注册、理财产品、金融工具和个人中心。登录和注册主要采用 shiro 来进行拦截和验证用户信息。用户可以通过发送 Ajax请求来买入或撤销理财产品。并且也可以在金融工具查看资金记录并且也可以申请网贷。用户可以在个人中心修改自己的个人信息，也可以查看购买的产品信息。对于管理员分为四个模块：用户信息管理、产品信息管理、权限和网贷管理。管理员可以通过发送 Ajax 请求来修改用户、产品和网贷的基本信息。通过 thymeleaf 和 shiro 对用户和管理员的权限进行管理。

二、软件架构
理财管理软件架构说明
后端：SpringBoot、Mybatis；
前端：thymeleaf、Html、CSS、BootStarp；
数据库：MySql；
服务器：Tomcat；
安全组件：shiro。

三、项目界面

1、登录

![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/213101_d46696c9_8607449.png "屏幕截图.png")

2、注册

![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/213029_b0610d5c_8607449.png "屏幕截图.png")

3、用户主页

![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/213210_c41e7397_8607449.png "屏幕截图.png")

4、用户理财模块

![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/213249_31074940_8607449.png "屏幕截图.png")

5、用户金融工具界面

![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/213327_43e5f264_8607449.png "屏幕截图.png")

6、用户个人中心界面

![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/213457_549daca8_8607449.png "屏幕截图.png")

7、管理员首页

![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/213527_31424c8a_8607449.png "屏幕截图.png")

8、管理员用户信息管理界面

![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/213633_4724e449_8607449.png "屏幕截图.png")

9、管理员理财产品管理界面

![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/213703_b7bacb8e_8607449.png "屏幕截图.png")

10、管理员权限管理界面

![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/213716_082c5d4a_8607449.png "屏幕截图.png")

11、管理员网贷管理界面

![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/213755_024bcfe7_8607449.png "屏幕截图.png")




